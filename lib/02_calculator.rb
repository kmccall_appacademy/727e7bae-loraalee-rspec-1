def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(array)
  num = 0
  if array.empty?
    return 0
  else
    array.each do |ele|
      num += ele
    end
    return num
  end
end
