def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, num=2)
  i = 1
  res = ""
  while i < num
    res << "#{string} "
    i += 1
  end
  res << string
end

def start_of_word(string, num)
  string[0...num]
end

def first_word(string)
  idx_of_space = string.index(" ")
  string[0...idx_of_space]
end

def titleize(string)
  little_words = ["and", "the", "over"]
  words = string.split(" ")

  words.each do |word|
    unless words.index(word) != 0 && little_words.include?(word)
      word[0] = word[0].capitalize
    end
  end

  words.join(" ")
end
