def index_of_first_vowel(string)
  vowels = "aeiou"
  string.each_char do |ele|
    if vowels.include? ele
      return string.index(ele)
    end
  end
end

def translate(string)
  vowels = "aeiou"
  words = string.split(" ")
  res = []

  words.each do |word|
    if vowels.include? word[0]
      word << "ay"
      res << word
    elsif word.include? "qu"
      qu_index = word.index("qu")
      word = word[qu_index + 2..-1] + "#{word[0...qu_index]}quay"
      res << word
    else
      idx = index_of_first_vowel(word)
      word = word[idx..-1] + word[0...idx] + "ay"
      res << word
    end
  end

  res.join(" ")
end
